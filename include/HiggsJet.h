#pragma once

#include <iostream>
#include <vector>
#include <utility>
#include <functional>

#include "Histogram.hh"

class HiggsJet {
    private:
        static constexpr double mh = 125;
        static constexpr double R = 0.4;
        static constexpr double g2 = 0.84;
        static constexpr double g1 = 0.2;
        static constexpr double Q12 = 2.4;
        static constexpr double correctiontoploop = 11*2.0/2.0;
        static double aerr;
        static double rerr;

        double ecm2;
        double b, bs2, qt, pj2, y1, y2, mu2;
        double s, t, u; 
        double x1, x2, xi1a, xi2a, x1a, x2a;
        double tb, ub;
        double mur, muf, muresum;

        double SudakovKernel(double, double, double, double, double, double, double) const;
        double Sudakov1gg(double, double) const;
        double Sudakov1qg(double, double) const;
        double Sudakov2gg(double, double) const;
        double Sudakov2qg(double, double) const;
        double sigma0gg() const;
        double sigma0qg() const;
        double H1gg(double) const;
        double H1qg(double) const;
        std::pair<double, double> PDFSplit() const;
        std::pair<double, double> CalcPdf() const;
        void FillHistograms(const std::vector<double>&, const std::vector<double>&);
        std::function<std::pair<double, double>()> Calculate;
        std::function<double()> MuR2;
        std::function<double()> MuF2;
        std::function<double()> MuResum2;

        std::pair<double, double> Resum() const;
        std::pair<double, double> Asym() const;

        bool write{false};
        bool resum{true};
        Histogram hist, histgg, histqg, hist_hpt, hist_ptj, hist_mhj;
        Histogram hist_maxTau;

    public :
        void WriteHistograms(bool _write) { write = _write; }
        double SetKinematics(const std::vector<double>&);
        double GetCalc(const std::vector<double>&, const double&);
        size_t NDims() { return resum ? 7 : 6; }

        HiggsJet(const YAML::Node &settings) {
            if(settings["Mode"].as<std::string>() == "Resum") {
                Calculate = [&]() { return Resum(); };
                resum = true;
            } else if(settings["Mode"].as<std::string>() == "Asym") {
                Calculate = [&]() { return Asym(); };
                resum = false;
            } else {
                throw std::logic_error("Invalid mode");
            }

            mur = settings["mur"].as<double>();
            muf = settings["muf"].as<double>();
            muresum = settings["muresum"].as<double>();
            if(settings["scale_mur"].as<std::string>() == "mh") {
                MuR2 = [&]() {
                    return mur*mur*mh*mh;
                };
            } else if(settings["scale_mur"].as<std::string>() == "pj") {
                MuR2 = [&]() {
                    return mur*mur*pj2; 
                };
            } else {
                throw std::logic_error("Invalid scale option");
            }
            if(settings["scale_muf"].as<std::string>() == "mh") {
                MuF2 = [&]() {
                    return muf*muf*mh*mh;
                };
            } else if(settings["scale_muf"].as<std::string>() == "pj") {
                MuF2 = [&]() {
                    return muf*muf*pj2;
                };
            } else {
                throw std::logic_error("Invalid scale option");
            }
            if(settings["scale_muresum"].as<std::string>() == "mh") {
                MuResum2 = [&]() {
                    return muresum*muresum*mh*mh;
                };
            } else if(settings["scale_muresum"].as<std::string>() == "pj") {
                MuResum2 = [&]() {
                    return muresum*muresum*pj2;
                };
            } else {
                throw std::logic_error("Invalid scale option");
            }

            const std::vector<double> bins_qt{0, 30, 60, 120, 13000};
            hist = Histogram(bins_qt, "qt");
            histgg = Histogram(bins_qt, "qt_gg");
            histqg = Histogram(bins_qt, "qt_qg");

            const std::vector<double> bins_ptj{30, 60, 90, 120, 350, 13000};
            hist_ptj = Histogram(bins_ptj, "ptj");

            const std::vector<double> bins_pth{0, 5, 10, 15, 20, 25, 30, 35, 45,
                                               60, 80, 100, 120, 140, 170, 200,
                                               250, 300, 450, 650, 13000};
            hist_hpt = Histogram(bins_pth, "hpt");

            const std::vector<double> bins_mhj{120, 220, 300, 400, 600, 900, 13000};
            hist_mhj = Histogram(bins_mhj, "mhj");

            const std::vector<double> bins_maxTau{0, 5, 15, 25, 40, 13000};
            hist_maxTau = Histogram(bins_maxTau, "maxTau");
        }
        ~HiggsJet() {
            std::cout << hist.Integral() << std::endl;
            hist.Save("qt");
            histgg.Save("qt_gg");
            histqg.Save("qt_qg");
            hist_hpt.Save("hpt");
            hist_ptj.Save("ptj");
            hist_mhj.Save("mhj");
            hist_maxTau.Save("maxTau");
        }
};
