#pragma once

#include <cmath>

constexpr double EulerGamma = 0.5772156649015328606065120900824024310421;
constexpr double pi=M_PI; 
constexpr double alf=1.0/137;
const double C1=2.0*exp(-EulerGamma);
constexpr double CA=3.0;
constexpr double CF=4.0/3;
constexpr double bm=1.5;
constexpr double LQCD2_5=0.22615*0.22615;
constexpr double nf=5.0;
constexpr double GF=1.16639e-5;
constexpr double Nc=3.0;
constexpr double LQCD2=LQCD2_5;
constexpr double Bb0=(11.0-(2.0/3)*nf);
constexpr double BB0=Bb0/12;
constexpr double Bb1=102.0-38.0*nf/3;
constexpr double GTB=0.3894e12;
constexpr double y2_l=-6;
constexpr double y2_u=6;
constexpr double b_l=0;
constexpr double b_u=10; 
