#include <iostream>
#include <cmath>
#include <string>
#include "gsl/gsl_sf_bessel.h"
#include "gsl/gsl_sf_dilog.h"
#include "LHAPDF/LHAPDF.h"

#define polylog  gsl_sf_dilog
#define Log log
#define Power pow

#include "Constants.h"
#include "HiggsJet.h"
#include "Vegas.hh"
#include "FourVector.hh"
#include "ThreeVector.hh"
#include "DoubleExponential.hh"

LHAPDF::PDF* pdf2 = LHAPDF::mkPDF("PDF4LHC15_nnlo_30", 0);
std::shared_ptr<Random> rnd;

double pdf(int iparton, double  x, double q2) {
    if(x >= 1 || x <= 0) return 0;
    else { 
        if(iparton==0) iparton = 21;
        return pdf2->xfxQ2(iparton, x, q2)/x;
    }
} 

double as(double mu2) {
    return pdf2 -> alphasQ2(mu2); 
}

constexpr double S=13000*13000;

double HiggsJet::aerr = 1E-16;
double HiggsJet::rerr = 1E-8;

double HiggsJet::SetKinematics(const std::vector<double> &x) {
    constexpr double bmin = 0;
    constexpr double bmax = 10;
    constexpr double db = bmax-bmin;
    constexpr double qtmin = 0;
    constexpr double qtmax = 200;
    constexpr double dqt = qtmax-qtmin;
    constexpr double pj2min = 30*30;
    constexpr double pj2max = 1000*1000;
    constexpr double dpj2 = pj2max-pj2min;
    constexpr double y1max = 4.4;
    constexpr double dy1 = 2*y1max;
    constexpr double y2max = 6;
    constexpr double dy2 = 2*y2max;

    double jac = 0;
    if(resum) {
        b = db*x[0]+bmin;
        qt = dqt*x[1]+qtmin;
        xi1a = x[2];
        xi2a = x[3];
        pj2 = dpj2*x[4]+pj2min;
        y1 = dy1*x[5]-y1max;
        y2 = dy2*x[6]-y2max;

        bs2 = b*b/(1+b*b/bm/bm);

        jac = db*dqt*dpj2*dy1*dy2;
    } else {
        qt = (dqt-5)*x[0]+qtmin + 5;
        xi1a = x[1];
        xi2a = x[2];
        pj2 = dpj2*x[3]+pj2min;
        y1 = dy1*x[4]-y1max;
        y2 = dy2*x[5]-y2max;

        jac = (dqt-5)*dpj2*dy1*dy2;
    }

    x1 = (sqrt(mh*mh+pj2)*exp(y2) + sqrt(pj2)*exp(y1))/sqrt(S);
    x2 = (sqrt(mh*mh+pj2)*exp(-y2) + sqrt(pj2)*exp(-y1))/sqrt(S);
    x1a = (sqrt(mh*mh+pj2)*exp(y2) + sqrt(pj2)*exp(y1))/sqrt(S);
    x2a = (sqrt(mh*mh+pj2)*exp(-y2) + sqrt(pj2)*exp(-y1))/sqrt(S);

    s = x1*x2*S;
    t = -x1*sqrt(S)*sqrt(pj2)*exp(-y1);
    u = -x2*sqrt(S)*sqrt(pj2)*exp(y1); 

    tb = mh*mh-t;
    ub = mh*mh-u;

    return jac;
}

double HiggsJet::SudakovKernel(double logmu2, double Q2,
                               double A1, double A2,
                               double B1, double M1, double D1) const {
    const double mu2 = exp(logmu2);
    const double alpi = as(mu2)/M_PI;

    const double dlQ = log(Q2)-logmu2;
    return alpi*((A1+alpi*A2)*dlQ+B1+M1+D1);
}

double HiggsJet::Sudakov1gg(double Q2, double mu2) const {
    constexpr double A1=CA;
    constexpr double A2=CA*((67.0/36.0-pi*pi/12.0)*CA-5.0/18.0*nf);

    constexpr double B1=-2*CA*(11-2/3.0*nf)/12.0;
    constexpr double M1=0;
    double D1=CA*log(1/R);

    double uplim = log(MuResum2());
    double dnlim = log(C1*C1/bs2);

    auto sudFunc = [&](double logMu2) {
        return SudakovKernel(logMu2, Q2, A1, A2, B1, M1, D1);
    };
    Utility::DoubleExponential sudIntegral(sudFunc);
    if(uplim > dnlim) return exp(-sudIntegral.Integrate(dnlim, uplim, aerr, rerr));
    return exp(sudIntegral.Integrate(uplim, dnlim, aerr, rerr));
}

double HiggsJet::Sudakov1qg(double Q2, double mu2) const {
    constexpr double A1=(CA+CF)/2.0;
    constexpr double A2=(CA+CF)/2.0*((67.0/36.0-pi*pi/12.0)*CA-5.0/18.0*nf);

    constexpr double B1=(-2*CA*(11-2/3.0*nf)/12.0-3*CF/2.0)/2.0;
    double M1=-(CA*Log(u/t))/2. + (CF*Log(u/t))/2.;
    double D1=CF*log(1/R);

    double uplim = log(MuResum2());
    double dnlim = log(C1*C1/bs2);

    auto sudFunc = [&](double logMu2) {
        return SudakovKernel(logMu2, Q2, A1, A2, B1, M1, D1);
    };
    Utility::DoubleExponential sudIntegral(sudFunc);
    if(uplim > dnlim) return exp(-sudIntegral.Integrate(dnlim, uplim, aerr, rerr));
    return exp(sudIntegral.Integrate(uplim, dnlim, aerr, rerr));
}

double HiggsJet::Sudakov2gg(double Q2, double b) const {
    return exp(-CA/CF*(g1*b*b+g2*(log(Q2/Q12)/2)*log(b*b/bs2)/2 ));
}

double HiggsJet::Sudakov2qg(double Q2, double b) const {
    return exp(-(CA/CF+1)/2.0*(g1*b*b+g2*(log(Q2/Q12)/2)*log(b*b/bs2)/2 ));
}

double HiggsJet::sigma0gg() const {
    double alpi = as(MuR2())/pi;
    double G2 = sqrt(2)*alpi*alpi*GF/9.0;

    double H0gg = Nc*(Nc*Nc-1)/s/t/u*(Power(s,4)+Power(t,4)+Power(u,4)+Power(mh,8));
    constexpr double Kgg = 1/4.0/(Nc*Nc-1.0)/(Nc*Nc-1.0);

    return 1/s/s*G2*pi*alpi/4.0*H0gg*Kgg*x1*x2;
}

double HiggsJet::sigma0qg() const {
    double alpi = as(MuR2())/pi;
    double G2 = sqrt(2)*alpi*alpi*GF/9.0;

    double H0qg = CA*CF/(-u)*(Power(s,2)+Power(t,2));
    constexpr double Kqg = 1/4.0/(Nc*Nc-1.0)/Nc;

    return 1/s/s*G2*pi*alpi/4.0*H0qg*Kqg*x1*x2;
}

double HiggsJet::H1gg(double x) const {
    double H0gg = Nc*(Nc*Nc-1)/s/t/u*(Power(s,4)+Power(t,4)+Power(u,4)+Power(mh,8));
    double deltaH1gg = (Power(mh,2)*Nc*(-1 + Power(Nc,2))*(Nc - nf)*(s*t*u + Power(mh,2)*(s*t + s*u + t*u)))/(3.*s*t*u);
    double adterm = BB0*12.0*log(MuR2()/x)*3.0/2.0;

    return
        CA*(
                R
                +67.0/9.0 - (23*nf)/54. + Power(pi,2)/2. + Power(log(tb/Power(mh,2)),2)
                - Power(log(-(tb/t)),2) - 2*log(-(t/s))*log(-(u/s)) + Power(log(ub/Power(mh,2)),2) - 
                Power(log(-(ub/u)),2) + log(Power(R,-2))*log(x/pj2) + Power(log(x/pj2),2) + 2*BB0*log(x/(pj2*Power(R,2)))-2*log(x/pj2)*log(x/s) + 2*polylog(1 - Power(mh,2)/s) + 2*polylog(t/Power(mh,2)) + 
                2*polylog(u/Power(mh,2)))
        + deltaH1gg/H0gg
        + correctiontoploop
        +adterm
        +5.5;
}

double HiggsJet::H1qg(double x) const {
    double H0qg = CA*CF/(-u)*(Power(s,2)+Power(t,2));
    double deltaH1qg = 2.0*(CA-CF)/2.0*CA*CF*(s+t);
    constexpr double correctiontoploop = 11*2.0/2.0;
    double adterm = BB0*12.0*log(MuR2()/x)*3.0/2.0;

    return
        20*BB0 + CF*(
                //      polylog(-1/R)+pi*pi/6.0+log(sin(R)*sin(R))*Log(sin(R)*sin(R))/8.0
                R
                -1.5 - (5*Power(pi,2))/6. + Log(tb/Power(mh,2))*Log(tb/Power(mh,2)) - Log(-(tb/t))*Log(-(tb/t))-log(x/pj2)*log(x/s)
                + 3*Log(-(u/x)) + Log(Power(R,-2))*Log(x/pj2) - Log(u/t)*Log(pj2/x) + Power(Log(x/pj2),2)/2. + 
                (3*Log(x/(pj2*Power(R,2))))/2. + 2*polylog(1 - Power(mh,2)/s) + 2*polylog(t/Power(mh,2))) + 
        CA*(7.0/3.0 + (4*Power(pi,2))/3. + Power(Log(ub/Power(mh,2)),2) - Power(Log(-(ub/u)),2) + Log(pj2/x)*Log(s/x) - 4*BB0*Log(-(u/x)) - 2*Log(-(t/x))*Log(-(u/x)) +
                Log(u/t)*Log(pj2/x) + Power(Log(x/pj2),2)/2. + 2*polylog(u/Power(mh,2)))
        + deltaH1qg/H0qg
        + correctiontoploop
        +adterm
        +5.5;
}

double theta(double x) {
    return x > 0 ? 1.0 : 0.0;
}

std::pair<double, double> HiggsJet::PDFSplit() const {
    double z1=x1/xi1a;
    double z2=x2/xi2a;

    double ffgg1 = 2.0*CA*(pdf(0, x1, MuF2()))
        * ((pdf(0, z2, MuF2())*theta(xi2a-x2)-pdf(0,x2,MuF2()))/(1-xi2a)
            + theta(xi2a-x2)*(pdf(0,z2,MuF2())/xi2a)*(1-xi2a)*(1+xi2a*xi2a)/xi2a);

    double ffgg2 = 2.0*CA*(pdf(0, x2, MuF2()))
        * ((pdf(0, z1, MuF2())*theta(xi1a-x1)-pdf(0,x1,MuF2()))/(1-xi1a)
            + theta(xi1a-x1)*(pdf(0,z1,MuF2())/xi1a)*(1-xi1a)*(1+xi1a*xi1a)/xi1a);

    double ffqg = nf*pdf(0, x1, MuF2())*(theta(xi2a-x2)*pdf(0,z2,MuF2()))*(xi2a*xi2a+(1-xi2a)*(1-xi2a))/xi2a;
    double ffgq = nf*pdf(0, x2, MuF2())*(theta(xi1a-x1)*pdf(0,z1,MuF2()))*(xi1a*xi1a+(1-xi1a)*(1-xi1a))/xi1a;

    double ffgg = ffgg1+ffgg2;

    for(int i = 1; i < 6; ++i) {
        ffqg += 2.0*CA*((pdf(0, z1, MuF2())*theta(xi1a-x1)-pdf(0,x1,MuF2()))/(1-xi1a)
              +theta(xi1a-x1)*pdf(0,z1,MuF2())/xi1a*(1-xi1a)*(1+xi1a*xi1a)/xi1a)*pdf(i, x2, MuF2());
        ffqg += CF*(pdf(0, x1, MuF2())
              *(pdf(i, z2, MuF2())*(1+xi2a*xi2a)/xi2a*theta(xi2a-x2)-2.0*pdf(i,x2,MuF2()))/(1-xi2a));
        ffgg += (4.0/3.0)*(theta(xi1a-x1)*(pdf(i,z1,MuF2())/xi1a)*(1+(1-xi1a)*(1-xi1a))/xi1a)*pdf(0,x2,MuF2());

        ffqg += 2.0*CA*((pdf(0, z1, MuF2())*theta(xi1a-x1)-pdf(0,x1,MuF2()))/(1-xi1a)
              +theta(xi1a-x1)*pdf(0,z1,MuF2())/xi1a*(1-xi1a)*(1+xi1a*xi1a)/xi1a)*pdf(-i, x2, MuF2());
        ffqg += CF*(pdf(0, x1, MuF2())
              *(pdf(-i, z2, MuF2())*(1+xi2a*xi2a)/xi2a*theta(xi2a-x2)-2.0*pdf(-i,x2,MuF2()))/(1-xi2a));
        ffgg += (4.0/3.0)*(theta(xi1a-x1)*(pdf(-i,z1,MuF2())/xi1a)*(1+(1-xi1a)*(1-xi1a))/xi1a)*pdf(0,x2,MuF2());

        ffgq += 2.0*CA*((pdf(0, z2, MuF2())*theta(xi2a-x2)-pdf(0,x2,MuF2()))/(1-xi2a)
              +theta(xi2a-x2)*pdf(0,z2,MuF2())/xi2a*(1-xi2a)*(1+xi2a*xi2a)/xi2a)*pdf(i, x1, MuF2());
        ffgq += CF*(pdf(0, x2, MuF2())
              *(pdf(i, z1, MuF2())*(1+xi1a*xi1a)/xi1a*theta(xi1a-x1)-2.0*pdf(i,x1,MuF2()))/(1-xi1a));
        ffgg += (4.0/3.0)*(theta(xi2a-x2)*(pdf(i,z2,MuF2())/xi2a)*(1+(1-xi2a)*(1-xi2a))/xi2a)*pdf(0,x1,MuF2());

        ffgq += 2.0*CA*((pdf(0, z2, MuF2())*theta(xi2a-x2)-pdf(0,x2,MuF2()))/(1-xi2a)
              +theta(xi2a-x2)*pdf(0,z2,MuF2())/xi2a*(1-xi2a)*(1+xi2a*xi2a)/xi2a)*pdf(-i, x1, MuF2());
        ffgq += CF*(pdf(0, x2, MuF2())
              *(pdf(-i, z1, MuF2())*(1+xi1a*xi1a)/xi1a*theta(xi1a-x1)-2.0*pdf(-i,x1,MuF2()))/(1-xi1a));
        ffgg += (4.0/3.0)*(theta(xi2a-x2)*(pdf(-i,z2,MuF2())/xi2a)*(1+(1-xi2a)*(1-xi2a))/xi2a)*pdf(0,x1,MuF2());
    }

    return {ffgg, ffqg+ffgq};
}

std::pair<double, double> HiggsJet::CalcPdf() const { 
    double mu2 = C1*C1/bs2;

    double xi1 = (1-x1)*xi1a+x1;
    double xi2 = (1-x2)*xi2a+x2;

    double z1 = x1/xi1;
    double z2 = x2/xi2;

    double pdfSum1 = 0, pdfSum2 = 0;
    for(size_t i = 1; i < 6; ++i) {
        pdfSum1 += pdf(i, xi1, mu2) + pdf(-i, xi1, mu2);
        pdfSum2 += pdf(i, xi2, mu2) + pdf(-i, xi2, mu2);
    }
    double alpi = as(mu2)/pi;
    double fgSum1 = pdf(0, x1, mu2)/(1-x1) + alpi*pdfSum1/xi1*(CF/2.0*z1);
    double fgSum2 = pdf(0, x2, mu2)/(1-x2) + alpi*pdfSum2/xi2*(CF/2.0*z2);

    double ffgg=(1-x1)*(1-x2)*fgSum1*fgSum2;

    double fg1 = alpi*pdf(0, xi1, mu2)/xi1*z1/2.0*(1-z1);
    double fg2 = alpi*pdf(0, xi2, mu2)/xi2*z2/2.0*(1-z2);

    double ffqg = 0, ffgq = 0;
    for(int i = 1; i < 6; ++i) {
        ffqg += (1-x1)*(1-x2)*fgSum1*(pdf(i, x2, mu2)/(1-x2)
              + alpi*pdf(i, xi2, mu2)/xi2*(2.0/3.0*(1-z2)) + fg2);
        ffqg += (1-x1)*(1-x2)*fgSum1*(pdf(-i, x2, mu2)/(1-x2)
              + alpi*pdf(-i, xi2, mu2)/xi2*(2.0/3.0*(1-z2)) + fg2);

        ffgq += (1-x1)*(1-x2)*fgSum2*(pdf(i, x1, mu2)/(1-x1)
              + alpi*pdf(i, xi1, mu2)/xi1*(2.0/3.0*(1-z1)) + fg1);
        ffgq += (1-x1)*(1-x2)*fgSum2*(pdf(-i, x1, mu2)/(1-x1)
              + alpi*pdf(-i, xi1, mu2)/xi1*(2.0/3.0*(1-z1)) + fg1);
    }

    return {ffgg, ffqg+ffgq};
}

std::pair<double, double> HiggsJet::Resum() const {
    std::pair<double, double> pdfResults = CalcPdf();
    double fgg = pdfResults.first;
    double fqg = pdfResults.second;

    double sigmaGG = 
        pi*2*qt*(1+as(MuR2())/2.0/pi*H1gg(MuResum2()))
        *GTB*sigma0gg()*Sudakov1gg(s, MuResum2()) * Sudakov2gg(s, b)
        *(b/(2.0*pi))*(gsl_sf_bessel_Jnu(0,qt*b))*fgg;

    double sigmaQG = 
        pi*2*qt*(1+as(MuR2())/2.0/pi*H1qg(MuResum2()))
        *GTB*sigma0qg()*Sudakov1qg(s, MuResum2()) * Sudakov2qg(s, b)
        *(b/(2.0*pi))*(gsl_sf_bessel_Jnu(0,qt*b))*fqg;

    return {sigmaGG, sigmaQG};
}

std::pair<double, double> HiggsJet::Asym() const {
    std::pair<double, double> pdfResults = PDFSplit();
    double fgg = pdfResults.first;
    double fqg = pdfResults.second;

    double prefactor = 2*qt*as(MuR2())/2.0/pi/qt/qt*GTB;
    double prefactor_gg = prefactor*sigma0gg();
    double sigmaGG = prefactor_gg*(2.0*CA*log(s/qt/qt)+CA*log(1/R/R))*pdf(0, x1, MuF2())*pdf(0, x2, MuF2());
    sigmaGG += prefactor_gg*fgg;

    double prefactor_qg = prefactor*sigma0qg();
    double pdfSum = 0;
    for(int i = 1; i < 6; ++i) {
        pdfSum += pdf(0, x1, MuF2())*(pdf(i, x2, MuF2()) + pdf(-i, x2, MuF2()));
        pdfSum += pdf(0, x2, MuF2())*(pdf(i, x1, MuF2()) + pdf(-i, x1, MuF2()));
    }
    double sigmaQG = prefactor_qg*((CA+CF)*log(s/qt/qt)+CF*log(1/R/R)+(CF-CA)*log(u/t))*pdfSum;
    sigmaQG += prefactor_qg*fqg;

    return {sigmaGG, sigmaQG};
}

double HiggsJet::GetCalc(const std::vector<double> &x, const double &wgt) {
    double psWgt = SetKinematics(x);

    if(x1 > 1 || x2 > 1) return 0;
    if(pj2 < qt*qt) return 0;

    std::pair<double, double> result = Calculate();
    double sigmaGG = result.first;
    double sigmaQG = result.second;

    double totWgt = (sigmaGG + sigmaQG)*psWgt;

    double phi_jet = 2*M_PI*rnd -> Get();
    double phi_qt = 2*M_PI*rnd -> Get();
    FourVector jet(sqrt(pj2)*cosh(y1), sqrt(pj2)*cos(phi_jet), sqrt(pj2)*sin(phi_jet), sqrt(pj2)*sinh(y1));
    double phx = sqrt(pj2)*cos(phi_jet)-qt*cos(phi_qt);
    double phy = sqrt(pj2)*sin(phi_jet)-qt*sin(phi_qt);
    double pht2 = phx*phx+phy*phy;
    double mth = sqrt(mh*mh+pht2);
    FourVector higgs(mth*cosh(y2), phx, phy, mth*sinh(y2));

    double phi_a = 2*M_PI*rnd -> Get();
    double cos_a = 2*rnd -> Get() - 1;
    double sin_a = sqrt(1-cos_a*cos_a);
    FourVector A1(mh/2, mh/2*sin_a*cos(phi_a), mh/2*sin_a*sin(phi_a), mh/2*cos_a);
    FourVector A2(-A1.Vec3(), mh/2);

    auto cm_boost = (higgs + jet).BoostVector();
    auto cm_mom = (higgs + jet).Boost(-cm_boost);
    auto higgs_cm = higgs.Boost(-cm_boost);
    auto jet_cm = jet.Boost(-cm_boost);
    A1 = A1.Boost(higgs_cm.BoostVector());
    A2 = A2.Boost(higgs_cm.BoostVector());
    A1 = A1.Boost(cm_boost);
    A2 = A2.Boost(cm_boost);

    // A1 = A1.Boost(higgs.BoostVector());
    // A2 = A2.Boost(higgs.BoostVector());
    constexpr double BrAA = 2.27e-3;
    totWgt *= BrAA;

    // Sort photons
    if(A1.Pt() < A2.Pt()) {
        auto tmp = A1;
        A1 = A2;
        A2 = tmp;
    }

    // Cuts
    if(A1.Pt() < 25 || A2.Pt() < 25) return 0;
    double A1y = std::abs(A1.Rapidity());
    double A2y = std::abs(A2.Rapidity());
    if(A1y > 2.37 || (A1y > 1.37 && A1y < 1.52)) return 0;
    if(A2y > 2.37 || (A2y > 1.37 && A2y < 1.52)) return 0;
    if(A1.Pt() < 0.35*higgs.M() || A2.Pt() < 0.25*higgs.M()) return 0;

    if(write) {
        double mTj = sqrt(jet.Pt2() + 0.111*R*R*jet.Pt2());
        double maxTau = mTj/(2.0*cosh(jet.Rapidity() - higgs.Rapidity()));
        FillHistograms({qt, higgs.Pt(), jet.Pt(), (higgs + jet).M(), maxTau}, {totWgt*wgt, sigmaGG*psWgt*wgt, sigmaQG*psWgt*wgt});
    }

    return totWgt;
}

void HiggsJet::FillHistograms(const std::vector<double> &point, const std::vector<double> &wgt) {
    hist.Fill(point[0], wgt[0]); 
    histgg.Fill(point[0], wgt[1]); 
    histqg.Fill(point[0], wgt[2]); 
    hist_hpt.Fill(point[1], wgt[0]);
    hist_ptj.Fill(point[2], wgt[0]);
    hist_mhj.Fill(point[3], wgt[0]);
    hist_maxTau.Fill(point[4], wgt[0]);
}

int main() {
    YAML::Node settings = YAML::LoadFile("run.yml");
    HiggsJet hj(settings["Process"]);
    Utility::AdaptiveMap map(hj.NDims());
    rnd = std::make_shared<Random>(settings["Vegas"]["iSeed"].as<size_t>());
    Vegas vegas(map, settings["Vegas"]);
    vegas.SetRand(rnd);
    auto func = [&](const std::vector<double> &x, const double &wgt) {
        auto niterations = settings["Vegas"]["iterations"].as<double>();
        return hj.GetCalc(x, wgt/niterations);
    };
    vegas(func);
    hj.WriteHistograms(true);
    vegas.Clear();
    vegas(func);
    return 0;
}
